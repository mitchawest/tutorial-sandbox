import express from 'express'
import bodyParser from 'body-parser'
import { MongoClient } from 'mongodb'

const app = express()
const client = new MongoClient('mongodb://localhost:27017')

app.use(bodyParser.json())

app.get('/users', async (req, res, next) => {
  try {
    const db = client.db('local')
    const collection = db.collection('users')
    const result = await collection.find({ ...req.query }).toArray()
    res.status(200).send(result)
  } catch (err) {
    next(err)
  }
})

app.post('/users', async (req, res, next) => {
  try {
    const db = client.db('local')
    const collection = db.collection('users')
    const writeData = req.body.map((listValue) => ({
      ...listValue
    }))
    await collection.insertMany(writeData)
    res.status(201).send(req.body)
  } catch (err) {
    next(err)
  }
})

app.use((err, req, res, next) => {
  console.error(err.stack)
  res.status(500).send('Something broke!')
  next()
})

const run = async () => {
  await client.connect()
  console.log('Connected to mongodb')
  await new Promise((resolve, reject) => {
    app.listen(3000, (err) => {
      if (err) {
        reject(err)
      }
      console.log('Server listening on port 3000 🚀🚀🚀')
      resolve()
    })
  })
}

run()
